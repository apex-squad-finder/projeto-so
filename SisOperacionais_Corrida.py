# -*- coding: utf-8 -*-
# importa a biblioteca threading.
import threading

#Execução da condição de corrida.

glob = 0 #Define uma variavel global com valor 0
 
def global_incremento(): #método para incremento de 1 para a função global

   global glob
   glob += 1

def tarefa_thread():  #método para chamar a função global_incremento, por um número especificado de vezes.
   for _ in range(100000):
      global_incremento()

def main():  #Criação dos 2 threads, inicialização com a função start() e aguardo da conclusão com a função join()

   global glob
   glob = 0
   
   thread1 = threading.Thread(target= tarefa_thread)
   thread2 = threading.Thread(target= tarefa_thread)

   thread1.start()
   thread2.start()

   thread1.join()
   thread2.join()



if __name__ == "__main__": #cria um número de interações e executa o print
   for i in range(10):
      main()
      print("{1} após a interação-> {0}".format(i,glob))
