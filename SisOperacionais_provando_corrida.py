# -*- coding: utf-8 -*-
# importa a biblioteca threading.
import threading

#Execução com o lock, resolvendo o problema de condição de corrida. Provando que a mesma estava acontecendo.

glob = 0 #Define uma variavel global com valor 0

def lock_global_incremento(): #método para incremento de 1 para a função global
	global glob
	glob+=1
def tarefa_threads(lock):   #chama o metodo lock, para poder bloquear o efeito da condição de corrida
	for _ in range(100000):
		lock.acquire()
		lock_global_incremento()
		lock.release()

def main(): #Criação dos 2 threads, inicialização e condição de aguardo até conclusão
	global glob
	glob = 0

	lock = threading.Lock()
	thread1 = threading.Thread(target = tarefa_threads, args = (lock,))
	thread2 = threading.Thread(target = tarefa_threads, args = (lock,))

	thread1.start()
	thread2.start()

	thread1.join()
	thread2.join()

if __name__ == "__main__": #cria um número de interações e executa o print
	for i in range(10): 
		main()
		print("{1} após a interação-> {0}".format(i,glob))
